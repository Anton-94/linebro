<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbPartnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lb_partner', function (Blueprint $table) {
            $table->increments('id');
            $table->text('fio');
            $table->text('email');
            $table->dateTime('date_in');
            $table->text('login');
            $table->text('password');
            $table->integer('status');
            $table->text('rPurse');
            $table->float('win');
            $table->float('paid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lb_partner');
    }
}
