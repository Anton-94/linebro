<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbBlockurlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lb_blockurl', function (Blueprint $table) {
            $table->increments('id');
            $table->text('status');
            $table->dateTime('date');
            $table->dateTime('date_start');
            $table->text('email');
            $table->float('price');
            $table->text('text');
            $table->text('url');
            $table->text('ip');
            $table->text('dop1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lb_blockurl');
    }
}
