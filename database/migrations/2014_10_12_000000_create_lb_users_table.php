<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lb_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fio',100);
            $table->string('email',60)->unique();
            $table->string('type',20);
            $table->string('login',20)->unique();
            $table->string('password',40);
            $table->text('access');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lb_users');
    }
}
