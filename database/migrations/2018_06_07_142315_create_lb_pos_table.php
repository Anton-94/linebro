<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lb_pos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type',10);
            $table->string('po',10);
            $table->integer('numbering');
            $table->text('description');
            $table->float('price');
            $table->integer('minprice');
            $table->integer('maxlength');
            $table->integer('ovr');
            $table->text('text');
            $table->text('url');
            $table->text('addtext');
            $table->text('waytext');
            $table->text('size');
            $table->text('width');
            $table->text('picture');
            $table->tinyInteger('antimat');
            $table->tinyInteger('captcha');
            $table->tinyInteger('statistics');
            $table->tinyInteger('active');
            $table->tinyInteger('alls');
            $table->tinyInteger('addurl');
            $table->tinyInteger('mypos');
            $table->integer('per_discount');
            $table->integer('val_discount');
            $table->integer('shows');
            $table->integer('clicks');
            $table->integer('add_clicks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lb_pos');
    }
}
