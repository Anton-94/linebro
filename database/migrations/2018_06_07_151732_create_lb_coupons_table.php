<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lb_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->text('partner_login');
            $table->text('code');
            $table->text('discount');
            $table->text('status');
            $table->text('who');
            $table->dateTime('when');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lb_coupons');
    }
}
