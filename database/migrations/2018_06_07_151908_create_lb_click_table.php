<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbClickTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lb_click', function (Blueprint $table) {
            $table->increments('id');
            $table->date('time_click');
            $table->text('ip');
            $table->integer('order_id');
            $table->integer('position_id');
            $table->integer('addPosition_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lb_click');
    }
}
