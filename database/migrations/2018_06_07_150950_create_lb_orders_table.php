<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLbOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lb_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->text('status');
            $table->integer('id_position');
            $table->dateTime('date');
            $table->dateTime('date_start');
            $table->text('name');
            $table->text('email');
            $table->text('type');
            $table->text('po');
            $table->text('do');
            $table->integer('shows');
            $table->integer('clicks');
            $table->text('param1');
            $table->text('param2');
            $table->text('param3');
            $table->text('param4');
            $table->float('price');
            $table->text('coupon');
            $table->text('newparam1');
            $table->text('newparam2');
            $table->text('newparam3');
            $table->text('newparam4');
            $table->text('refferal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lb_orders');
    }
}
