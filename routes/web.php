<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('install','InstallController@index');

Route::get('step1', function () {
    return view('install.formForDB');
})->name('step1');

Route::post('step1', 'InstallController@formForDB');

Route::get('error_step2', function() {
    return view('install.errors.step2');
})->name('error_step2');

Route::get('step3', function () {
    return view('install.endInstall');
})->name('step3');

Auth::routes();

