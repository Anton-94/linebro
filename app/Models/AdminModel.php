<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminModel extends Model
{
    public $incrementing = false;

    protected $table = 'lb_admin';
}
