<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    public function redirectPath()
    {
        return route('step3');
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|alpha_num|min:4|max:20',
            'email' => 'required|email|unique:lb_users',
            'fio' => 'required|min:8|max:100',
            'login' => 'required|min:4|max:20'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
//     * @return \App\Models\User
     */
    protected function create( array $data)
    {
        $user = User::create([
            'fio' => $data['fio'],
            'email' => $data['email'],
            'type' => 'admin',
            'login' => $data['login'],
            'password' => bcrypt($data['password']),
            'access' => '2/1/1/1/1/1/1/1/1/1/1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return $user;
    }
}
