<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InstallController extends Controller
{
    public function index()
    {
        return view('install.index');
    }

    public function formForDB(Request $request)
    {
        $db_settings = [
            'DB_HOST' => $request->input('bd_host'),
            'DB_DATABASE' => $request->input('bd_name'),
            'DB_USERNAME' => $request->input('bd_user'),
            'DB_PASSWORD' => $request->input('bd_pass')
        ];

        try{
//            $this->createNewConfig($db_settings);
            $this->setValueForEnv($db_settings);
        }catch(\Exception $e){
            return redirect(route('error_step2'));
        }

        return redirect(route('register'));
    }


//    private function createNewConfig($db_settings)
//    {
//        $file = config_path('lb_database.php');
//        $fp = fopen($file, "w");
//            $current = "<?php ";
//            $current .= "\n/** имя базы данных для скрипта ЛайнБро */";
//            $current .= "\n" . "define('LB_DB_NAME','" . $db_settings['DB_DATABASE'] . "');";
//            $current .= "\n/** имя пользователя MySQL */";
//            $current .= "\n" . "define('LB_DB_USER','" . $db_settings['DB_USERNAME'] . "');";
//            $current .= "\n/** пароль к базе данных MySQL */";
//            $current .= "\n" . "define('LB_DB_PASSWORD','" . $db_settings['DB_PASSWORD'] . "');";
//            $current .= "\n/** имя сервера MySQL */";
//            $current .= "\n" . "define('LB_DB_HOST','" . $db_settings['DB_HOST'] . "');";
/*            $current .= "\n" . "?>";*/
//        fwrite($fp, $current);
//        fclose($fp);
//    }

    private function setValueForEnv($db_settings)
    {
        $path = base_path('.env');

        if (file_exists($path)) {
            foreach ($db_settings as $key => $v){
                file_put_contents($path, str_replace(
                    $key . '=' . env($key), $key . '=' . $v, file_get_contents($path)
                ));
            }
        }
    }


}
