@extends('layouts.admin')

@section('content')
    <div class="wrapper_in">
        <h1>Авторизация</h1>
        <form method="POST" class="form_in" >
            {{csrf_field()}}
            <p><label for="user_login">Имя пользователя<br><input type="text" name="login" id="user_login" value="<?php print isset($_POST["login"]) ? $_POST["login"] : "" ; ?>" required></label></p>
            <p><label for="user_password">Пароль<br><input type="password" name="password" id="user_password" value="" required></label></p>
            <p><input type="submit" name="submit" style="margin-top:2px;" value="Войти"></p>
        </form>
    </div>
    <div class="heyPassword"><a href="#">Забыли пароль?</a></div>
    <div class="heyPassword"><a href="#">&larr; На страницу продажи</a></div>
@endsection
