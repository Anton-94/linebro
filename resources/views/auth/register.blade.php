@extends('install.layout')

@section('content')
    <h2>Вы успешно установили связь с MySql</h2>
    Пожалуйста, укажите следующую информацию.
    <form method="POST" action="{{ route('register') }}">
        {{csrf_field()}}
        <table class="table_install">
            <tr>
                <td style="width:216px;">Придумайте логин</td>
                <td style="width:200px;"><input type="text" name="login" title="Только латинские буквы и цифры" pattern="^[a-zA-Z0-9]+$" required/></td>
                @if ($errors->has('login'))
                    <td style="color: red;">{{ $errors->first('login') }}</td>
                @endif
            </tr>
            <tr>
                <td>Придумайте пароль</td>
                <td><input type="password" name="password" title="Только латинские буквы и цифры" pattern="^[a-zA-Z0-9]+$" required/></td>
                @if ($errors->has('password'))
                    <td style="color: red;">{{ $errors->first('password') }}</td>
                @endif
            </tr>
            <tr>
                <td>Рабочий Email</td>
                <td><input type="email" name="email" required/></td>
                @if ($errors->has('email'))
                    <td style="color: red;">{{ $errors->first('email') }}</td>
                @endif
            </tr>
            <tr>
                <td>Имя Фамилия</td>
                <td><input type="text" name="fio" required/></td>
                @if ($errors->has('fio'))
                    <td style="color: red;">{{ $errors->first('fio') }}</td>
                @endif
            </tr>
        </table>
        <input type="submit" name="bd_instal"  value="Подтвердить">

    </form>
@endsection
