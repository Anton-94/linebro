@extends('install.layout')

@section('content')
    <h2 style="color:green">Вы успешно установили LineBro</h2>
    Поздравляем! Процесс установки скрипта <u>ЛайнБро</u> успешно завершен.
    Ваши данные для входа в административную панель:
    <table class="table_install" style="margin-bottom:30px;background:#f4fff2; border:1px solid silver;">
        <tr>
            <td colspan="2" style="width:116px;padding:10px;"><a href="">Вход в панель</a></td>
        </tr>
    </table>
    Теперь вам необходимо зарегистрироваться на официальном сайте <u>linebro.ru</u> и получить ключ доступа.
    <form method="post" action="http://linebro.ru/registration/">
        <input type="hidden" name="db_name" value="'.LB_DB_NAME.'">
        <div class="white_big" align="right" style="margin-right:20px;"><input type="submit" value="Получить ключ"></div>
    </form>
@endsection