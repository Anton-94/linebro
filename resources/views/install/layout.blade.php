<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow" />

    <link rel="stylesheet" href="{{url('css/normalize.css')}}"/>
    <link rel="stylesheet" href="{{url('css/style.css')}}"/>


    <!--[if lt IE 9]>
    <script src="common/scripts/html5shiv.js"></script>
    <![endif]-->
    <title>Вас приветствует установщик скрипта LineBro!</title>
</head>
<body>
    <div class="logo"></div>
        <section class="wrapper_install">
            <div class="action_container">
                @section('content')
                @show
                    <div class="clear"></div>
            </div>
        </section>
</body>
</html>