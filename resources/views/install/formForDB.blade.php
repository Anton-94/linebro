@extends('install.layout')

@section('content')
    <h2>Вас приветствует мастер установки LineBro</h2>
    Пожалуйста введите данные для подключения к базе данных
    <form method="post" >
        {{ csrf_field() }}
        <table class="table_install">
            <tr>
                <td style="width:200px;">Имя базы данных</td>
                <td style="width:200px;"><input type='text' name="bd_name" value="" placeholder="linebro" required/></td>
                <td style="width:300px;">Имя базы данных, в которую вы хотите установить LineBro</td>
            </tr>
            <tr>
                <td>Имя пользователя</td>
                <td><input type='text' name="bd_user" value="" placeholder="username" required/></td>
                <td>Имя пользователя в базе данных</td>
            </tr>
            <tr>
                <td>Пароль</td>
                <td><input type='password' name="bd_pass" value="" placeholder="password" required/></td>
                <td>Пароль пользователя в базе данных</td>
            </tr>
            <tr>
                <td>Сервер базы данных</td>
                <td><input type='text' name="bd_host" value="localhost" placeholder="localhost" required/></td>
                <td>Если <b>localhost</b> не работает, обратитесь в службу поддержки хостинг-провайдера.</td>
            </tr>
        </table>
        <input type="submit" name="bd_install" value="Подтвердить">
    </form>
@endsection