<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="robots" content="noindex,nofollow" />

        <link rel="stylesheet" href="{{url('themes/default/css/normalize.css')}}"/>
        <link rel="stylesheet" href="{{url('themes/default/css/admin.css')}}"/>

        <link href="{{url('admin/addons/lightbox/css/lightbox.css')}}" type="text/css" rel="stylesheet">
        <link href="{{url('admin/addons/modern-menu/modern-menu.css')}}" type="text/css" rel="stylesheet">

    </head>
    <body>

        @section('content')

        @show

    </body>




    <script type="text/javascript" src="/linebro/common/js/jquery-1.7.2.js"></script>
    <script type="text/javascript" src="/linebro/common/js/linebro.js"></script>

    <script type="text/javascript" src="/linebro/admin/addons/lightbox/js/lightbox.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('a[data-lightbox]').lightbox();
        })
    </script>

    <!--[if lt IE 9]>
    <script src="common/js/html5shiv.js"></script>
    <![endif]-->
